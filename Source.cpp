#include<iostream>
#include<fstream>
#include <string.h>
using namespace std;

class Termo
{
private:
	char* eticheta;
	float tempcitita;
	float tempdorita;
public:

	Termo()
	{
		this->setEticheta("");
		this->setTempDorita(0);
		this->setTempCitita(0);
	}

	Termo(char* eticheta, float tempcitita, float tempdorita)
	{
		this->setEticheta(eticheta);
		this->setTempCitita(tempcitita);
		this->setTempDorita(tempdorita);
	}
	~Termo()
	{
		delete eticheta;
	}
	Termo(const Termo& term)
	{
		this->setEticheta(term.eticheta);
		this->setTempCitita(term.tempcitita);
		this->setTempDorita(term.tempdorita);
	}
	void setTempCitita(float tempcitita)
	{
		this->tempcitita = tempcitita;
	}
	void setTempDorita(float tempdorita)
	{
		this->tempdorita = tempdorita;
	}
	void setEticheta(char* eticheta)
	{
		this->eticheta = new char[strlen(eticheta) + 1];
		strcpy(this->eticheta, eticheta);
	}

	float getTempDorita()
	{
		return this->tempdorita;
	}
	float getTempCitita()
	{
		return this->tempcitita;
	}
	char* getEticheta()
	{
		return this->eticheta;

	}
	//virtual float tempMedieCitita();
	friend ostream &operator <<(ostream& out, Termo& term)
	{
		out << term.eticheta << " " << term.tempcitita << " " << term.tempdorita << endl;
		return out;
    }
	friend istream &operator >>(istream& in, Termo& term)
	{
		char buffer[200];
		in >> buffer;
		term.setEticheta(buffer);
		in >> term.tempcitita;
		in >> term.tempdorita;
		return in;
	}
	Termo& operator -- ()
	{
		this->setTempDorita(this->getTempDorita() - 1);
		return *this;
	}
};

class UCC
{
private:
	char* locatie;
	int nrTermostate;
	Termo* listatermo[12];
public:
	UCC()
	{
		nrTermostate = 0;
	}
	UCC(char* locatie, int nrTermostat, Termo* listaTermo[])
	{
		nrTermostate = 0;
		this->setLocatie(locatie);
	
		for (int i = 0; i < nrTermostat; i++)
		{
			this->addTermostate(listatermo[i]);
		}
	}

	UCC(const UCC& u)
	{
		this->setLocatie(u.locatie);
		this->setNrTermostate(u.nrTermostate);
		for (int i = 0; i < u.nrTermostate; i++)
		{
			this->addTermostate(u.listatermo[i]);
		}
	}
	void setLocatie(char* locatie)
	{
		this->locatie = new char[strlen(locatie) + 1];
		strcpy(this->locatie, locatie);
	}
	void setNrTermostate(int nr)
	{
		this->nrTermostate = nr;

	}

	char* getLocatie()
	{
		return this->locatie;
	}
	int getNrTermostate()
	{
		return this->nrTermostate;
	}
	void addTermostate(Termo* term)
	{
		listatermo[nrTermostate++] = term;
	}
	friend  ostream &operator <<(ostream& output, UCC& uc)
	{
		
		output << uc.locatie << " " << uc.nrTermostate << endl;
		for (int i = 0; i < uc.nrTermostate; i++)
			output << *uc.listatermo[i] ; 
		return output;
	}
};







int main()
{

	Termo t1("dormitor 1", 22.5, 23.5);
	Termo t2 = t1;
	Termo t3,t4;
	t3 = t1;
	t3.setEticheta("bucatarie");
	cout << t1 << endl << t2 << endl << t3 << endl;
	/*cin >> t4;
	--t4;
	cout << t4;*/
	Termo* term[2];
	term[0] = &t1;
	term[1] = &t2;
	
	/*UCC u1("etaj 1", 3, term);
	cout << u1;*/

	return 0;
}